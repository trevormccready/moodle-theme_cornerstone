<?php
$HTML = theme_cornerstone_get_settings($OUTPUT, $PAGE);
$bodyclasses = array();
if ($HTML->cufullscreenpreference == 1) {
	$bodyclasses[] = 'fullscreen';
}
if ( !empty($CFG->maintenance_enabled)) {
        $bodyclasses[] = 'maintenance_mode';
} else if ( isset($CFG->maintenance_later) ) {
        $bodyclasses[] = 'maintenance_warn';
}
$showsidepre = '';
$showsidepost = '';
$hassidepre = $PAGE->blocks->region_has_content('side-pre', $OUTPUT);
$hassidepost = $PAGE->blocks->region_has_content('side-post', $OUTPUT);
if( $hassidepre ) {
	if ( empty($PAGE->layout_options['nonavbar']) && $PAGE->blocks->region_completely_docked('side-pre', $OUTPUT) ) {
		$showsidepre = false;
	} else {
		$showsidepre = true;
	}
}
if( $hassidepost ) {
	if ( empty($PAGE->layout_options['nonavbar']) && $PAGE->blocks->region_completely_docked('side-post', $OUTPUT) ) {
		$showsidepost = false;
	} else {
		$showsidepost = true;
	}
}

if ($showsidepre && !$showsidepost) {
	$bodyclasses[] = 'side-pre-only';
	$bodyclasses[] = 'side-pre';
} else if (!$showsidepre && $showsidepost) {
	$bodyclasses[] = 'side-post-only';
	$bodyclasses[] = 'side-post';
} else if (!$showsidepre && !$showsidepost) {
	$bodyclasses[] = 'content-only';
} else {
	$bodyclasses[] = 'side-pre';
	$bodyclasses[] = 'side-post';
}

echo $OUTPUT->doctype(); ?>
<html <?php echo $OUTPUT->htmlattributes() ?>>
<head>
	<!-- Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-P9GR5S8');</script>
	<!-- End Google Tag Manager -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<title><?php echo $PAGE->title ?></title>
	
	<link rel="shortcut icon" href="<?php echo $OUTPUT->favicon(); ?>" />
	
	<?php echo $OUTPUT->standard_head_html() ?>
	
	<!-- Load the BRANDON GROTESQUE font among others from Adobe's Typekit -- Requires permissions by domain -->
	<script src="https://use.typekit.net/ndq0ppg.js"></script>
	<script>try{Typekit.load({ async: true });}catch(e){}</script>
</head>

<body id="<?php echo $PAGE->bodyid; ?>" onload="loadMyCourseList();" class="site <?php echo $PAGE->bodyclasses.' '.join(' ',$bodyclasses); ?>">
<?php include_once(dirname(__FILE__).'/analyticstracking.php'); ?>
<?php echo $OUTPUT->standard_top_of_body_html() ?>

<!-- NAVBAR -->
<?php if (empty($PAGE->layout_options['nonavbar'])) {
	require_once(dirname(__FILE__).'/navbar.php');
} ?>

<!-- HEADER -->
<?php if ($PAGE->heading || (empty($PAGE->layout_options['nonavbar']) && $PAGE->hasnavbar())) {
	require_once(dirname(__FILE__).'/header.php');
} ?>

<?php if (!empty($HTML->alert)) { echo $HTML->alert; } ?>

<section id="page" class="site-content">
	<?php 
	if ( isloggedin() ) {
		if (($hassidepre && !$hassidepost) || (!$hassidepre && $hassidepost))  {
			$blockregionclass="col-sm-3";
			$maincontentclass="col-sm-9";
		} else if ($hassidepre && $hassidepost) {
			$blockregionclass="col-sm-3";
			$maincontentclass="col-sm-6";
		} else {
			$maincontentclass="col-sm-12";
		}
	} else {
		if ( $hassidepre || $hassidepost ) {
			$blockregionclass="col-sm-4";
			$maincontentclass="col-sm-8";
		}
	}
	?>
	<?php if ( isloggedin() ) { ?>
	<aside id="cu-block-region-pre" class="region-pre-container <?php echo $blockregionclass; ?>">
		<?php if ($hassidepre) { ?>
			<div id="region-pre" class="block-region">
				<div class="region-content"><?php echo $OUTPUT->blocks_for_region('side-pre') ?></div>
			</div>
		<?php } ?>
	</aside>
	<?php } ?>
	
	<?php if ( isloggedin() ) { ?>
	<main id="page-content" class="<?php echo $maincontentclass; ?>">
		<div id="region-main-wrap">
			<div id="region-main">
				<div class="region-content">
					<?php echo $OUTPUT->main_content(); ?>
				</div>
			</div> <!-- CLOSE region-main -->
		</div> <!-- CLOSE region-main-wrap -->
	</main>
	<?php } else { ?>
	<main id="page-content" class="col-sm-8">
		<div id="welcome_bg">
		<div id="welcome_wrapper">
			<div id="welcome_box">
				<h2>Welcome to Moodle.</h2>
				<div class="button"><a href="<?php echo $CFG->wwwroot; ?>/login" title="Login to CU Moodle">LOG IN</a></div>
				<div class="iewarning alert alert-error"><h3>You appear to using Internet Explorer<br/>CU does not support this browser when using Moodle.</h3><hr/>(<?php echo $_SERVER['HTTP_USER_AGENT']; ?>)</div>
			<div>
			<div style="display: inline;">For Moodle we recommend:</div>
			<ul>
				<li style="display: inline-block;"><a href="https://www.google.com/chrome/" target="_blank" title="Get Google Chrome"><img src="<?php echo $OUTPUT->pix_url('chrome', 'theme'); ?>" alt="Google Chrome Logo" width="18" height="18" style="float: left; margin: 0px 0.5em 0px 0px;" class="img-responsive">Google Chrome</a></li>
			<!--	<li style="display: inline-block;"><a href="https://www.getfirefox.com/" target="_blank" title="Get Mozilla Firefox"><img src="<?php echo $OUTPUT->pix_url('firefox', 'theme'); ?>" alt="Mozilla Firefox Logo" width="18" height="18" style="float: left; margin: 0px 0.5em 0px 0px;" class="img-responsive">Mozilla Firefox</a></li> -->
			</ul>
			</div>
		</div>
		</div></div>
	</main>
	<?php } ?>
	
	<aside id="cu-block-region" class="<?php if ( isloggedin() ) { echo 'region-pre-container region-post-container ' . $blockregionclass; } else { echo $blockregionclass; } ?>">
		<?php if ( !isloggedin() ) { ?>
			<div id="cu-frontpage-main-content"><?php echo $OUTPUT->main_content(); ?></div>
		<?php if ( $hassidepre ) { ?>
			<div id="region-pre" class="block-region">
				<div class="region-content"><?php echo $OUTPUT->blocks_for_region('side-pre') ?></div>
			</div>
		<?php } ?>
		<?php } ?>
		<?php if ($hassidepost) { ?>
			<div id="region-post" class="block-region">
				<div class="region-content"><?php echo $OUTPUT->blocks_for_region('side-post') ?></div>
			</div>
		<?php } ?>
	</aside>
</section>
	
<!-- PAGE FOOTER -->
<?php if (empty($PAGE->layout_options['nofooter'])) {
	require_once(dirname(__FILE__).'/footer.php');
} ?>

<!--  DEBUG INFORMATION -->
<?php require_once(dirname(__FILE__).'/debug.php'); ?>

<?php echo $OUTPUT->standard_end_of_body_html() ?>
<script type="text/javascript">
function loadMyCourseList() {
	// Add custom controls to the My Moodle Page 
	var mymoodlepage = document.getElementById("page-my-index");
	var mycoursesblock = getElementsByClass("block_course_overview", mymoodlepage, "div");
	if (mycoursesblock[0] != null) {
		var mycourseslist = getElementsByClass("content", mycoursesblock[0], "div");		
		mycourseslist[0].setAttribute("id", "mycourselist");
		var mycourseboxes = getElementsByClass("coursebox", mycourseslist[0], "div");
		var closedcourses = 0;
		var courseswithalerts = 0;
		// If there are course alerts, display an icon to show/hide assignment and forum reminders and notifications
		for (var i = 0; i < mycourseboxes.length; i++) {
			var courseboxid = mycourseboxes[i].id;
			var courseboxh2 = mycourseboxes[i].getElementsByTagName("h2");
			var coursetitle = courseboxh2[0].getElementsByTagName("a");
			var coursealert = getElementsByClass("activity_info", mycourseboxes[i], "div");
			if (coursealert.length > 0) {
				coursetitle[0].style.float = "left";
				var coursealertbutton = generateElement(courseboxh2[0], "i", null, "coursealerts", null);
				coursealertbutton.setAttribute("title", "You have assignments that need attention. Click to show/hide the overview.");
				coursealertbutton.setAttribute("class", "fa fa-bell");
				coursealertbutton.setAttribute("onClick", "javascript:theme_cornerstone_coursealert_toggle(this)");
				courseswithalerts++;
			}	
		}
		// Modify the message indicating how many hidden courses there are.
		var hiddencoursemessage = getElementsByClass("notice", mycourselist[0], "div");
		if ( hiddencoursemessage.length > 0 ) {
			if ( hasClass(document.body, "editing") ) {
				if ( hiddencoursemessage.length > 1 ) {
					hiddencoursemessage[1].innerHTML = hiddencoursemessage[1].innerHTML;
				}
			} else {
				hiddencoursemessage[0].innerHTML = hiddencoursemessage[0].innerHTML + ". Customize this page to change how many courses should be displayed.";
			}
		}
		// Add a course search box and browse course link
		cu_morecourses = generateElement(mycourseslist[0], "div", "cu_morecourses");
		cu_coursesearch = generateElement(cu_morecourses, "form", "cu_block_coursesearch");
		cu_coursesearch.setAttribute("action", "<?php echo $CFG->wwwroot; ?>/course/search.php");
		cu_coursesearch.setAttribute("method", "get");
		cu_coursesearch.setAttribute("_lpchecked", "1");
		cu_coursesearch_fieldset = generateElement(cu_coursesearch, "coursesearch_fieldset", "coursesearchbox invisiblefieldset");
		cu_search_label = generateElement(cu_coursesearch_fieldset, "label", null, null, "Search all CU courses: ");
		cu_search_label.setAttribute("for", "coursesearchbox");
		cu_search_textfield = generateElement(cu_coursesearch_fieldset, "input", "coursesearchbox");
		cu_search_textfield.setAttribute("type", "text");
		cu_search_textfield.setAttribute("size", "20");
		cu_search_textfield.setAttribute("name", "search");
		cu_search_button = generateElement(cu_coursesearch_fieldset, "input");
		cu_search_button.setAttribute("type", "submit");
		cu_search_button.setAttribute("value", "Go");
		cu_browsecourses = generateElement(cu_morecourses, "a", "browsecourses", "browsecourseslink button", "Browse Course Directory");
		cu_browsecourses.setAttribute("href", "<?php echo $CFG->wwwroot; ?>/course/index.php");
	}
}
</script>
</body>
</html>
