<?php 
// Setup help icon overlays.
        $this->page->requires->yui_module('moodle-core-popuphelp', 'M.core.init_popuphelp');
        $this->page->requires->strings_for_js(array(
            'morehelp',
            'loadinghelp',
        ), 'moodle');
?>
<header id="page-header">
	<?php if (empty($PAGE->layout_options['nonavbar']) && $PAGE->has_navbar()) { ?>
	<div class="navbar clearfix">
		<div class="breadcrumb pull-left hidden-xs"><?php echo $OUTPUT->navbar(); ?></div>
		<div id="cu-fullscreen-title" class="pull-left"><?php echo $cupageheading; ?></div>
		<div id="cu-nav-actionlinks">
			<ul>
	    		<li id="cu-fullscreen" title="Fullscreen On/Off">
	    			<i onclick="theme_cornerstone_fullscreen_toggle();" class="fa fa-expand fa-2x clickable"></i>
	    			<i onclick="theme_cornerstone_fullscreen_toggle();" class="fa fa-compress fa-2x clickable"></i>
	    		</li>
	    		<li id="cu-print" title="Print Page Content" style="display: none;"><a href="#" onclick="window.print();"><i class="fa fa-print fa-2x"></i></a></li>
	    	</ul>
			<div class="navbutton pull-right"><?php echo $PAGE->button; ?></div>
		</div>
	</div>
	<?php } ?>
</header>
