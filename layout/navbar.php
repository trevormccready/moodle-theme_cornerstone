<?php 
$context = context_course::instance($COURSE->id);
$course_shortname = explode(' ',$COURSE->shortname);
foreach(core_plugin_manager::instance()->get_plugins_of_type('block') as $pluginblock){
	if ($pluginblock->name == 'intelligent_learning') {
		$has_ilp_access = has_capability('block/intelligent_learning:edit', $context);
		break;
	} else {
		$has_ilp_access = false;
	}
}

if ($PAGE->pagelayout == "frontpage" || $PAGE->pagelayout == "mydashboard") { 
	$cudockclass="";
	$cupageheading="";
} elseif ($PAGE->pagelayout == "admin" && $COURSE->id == 1) {
	$cudockclass="";
	$adminname=get_string('administration','core');
	$cupageheading="<h1>{$adminname}</h1>";
} else {
	$cudockclass="visible";
	if ($COURSE->id != 1) {
		$cupageheading="<h1>{$COURSE->fullname}<span class='caret'></span></h1>";
	} else {
		$cupageheading="<h1>{$PAGE->heading}</h1>";
	}
} 
$rootcategory = theme_cornerstone_get_root_category($PAGE);
$curole = theme_cornerstone_get_course_role($PAGE);
?>
<nav class="navbar navbar-default navbar-fixed-top <?php echo $PAGE->pagelayout; ?>">
	<div class="container-fluid" id="cu-sitenav">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#cu-navbar">
        	<span class="sr-only">Toggle navigation</span>
        	<span class="icon-bar"></span><span class="icon-bar"></span>
        	<span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="<?php echo $CFG->wwwroot; ?>">CU<span class="cu-gold-color">Moodle</span></a>
        <?php if( !empty($HTML->subbrand)) {
        	echo $HTML->subbrand; 
        } ?>
      </div>
        <ul id="cu-navbar" class="nav navbar-nav navbar-right collapse">
          <?php if (isloggedin() && empty($PAGE->layout_options['nonavbar'])) { ?>
          <li id="cu-usermenu" class="dropdown cu-accent-lt-bg pull-right"><a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><?php echo $USER->firstname . ' ' . $USER->lastname; ?><span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="<?php echo $CFG->wwwroot; ?>/my" title="Dashboard"><i class="fa fa-graduation-cap" aria-hidden="false" aria-label="Go to My Moodle"></i>My Moodle</a></li>
              <?php if (!empty($HTML->emaillink)) { ?><li><?php echo $HTML->emaillink; ?></li><?php } ?>   
              <?php if (!empty($HTML->portallink)) { ?><li><?php echo $HTML->portallink; ?></li><?php } ?>
			  <li class="divider hidden-xs"></li>
              <li><a href="<?php echo $CFG->wwwroot; ?>/grade/report/overview/index.php" title="My Grades">Grades</a></li>
			  <li><a href="<?php echo $CFG->wwwroot; ?>/message/index.php" title="My Messages">Messages</a></li>
              <li><a href="<?php echo $CFG->wwwroot; ?>/user/preferences.php" title="My Preferences">Preferences</a></li>              
              <li><a href="<?php echo $CFG->wwwroot; ?>/user/profile.php?id=<?php echo $USER->id?>" title="My Profile">View Profile</a></li>
              <li class="divider"></li>
              <li><a href="<?php echo $CFG->wwwroot; ?>/login/logout.php?sesskey=<?php echo $USER->sesskey; ?>" title="Logout"><i class="fa fa-sign-out" aria-hidden="false" aria-label="Log out"></i>Log out</a></li>
              <?php 
                $branchlabel = '';
                $branchurl = '';
                if (is_role_switched($COURSE->id)) {
                    $branchlabel = get_string('switchrolereturn');
                    $branchurl = new moodle_url('/course/switchrole.php', array('id' => $COURSE->id, 'sesskey' => sesskey(), 'switchrole' => 0, 'returnurl' => $this->page->url->out_as_local_url(false)));
                } else {
                    $roles = get_switchable_roles($context);
                    if (is_array($roles) && (count($roles) > 0)) {
                        $branchlabel = get_string('switchroleto');
                        $branchurl = new moodle_url('/course/switchrole.php', array('id' => $COURSE->id, 'switchrole' => -1, 'returnurl' => $this->page->url->out_as_local_url(false)));
                    }
                }
              ?>
                <?php if ($branchlabel) { ?>
                    <li class="divider"></li>
                    <li><a href="<?php echo $branchurl; ?>"><?php echo $branchlabel; ?></a></li>
                <?php } ?>
            </ul>
          </li>
          <?php } ?>        
		  <li id="cu-navbar-helpdropdown" class="dropdown pull-right"><a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Help<span class="caret"></span></a>
            <ul class="dropdown-menu multi-column columns-2" role="menu">
            	<?php if($PAGE->pagelayout != "maintenance") { ?>
            	<div class="row">
            		<div class="col-sm-6">
            			<ul class="multi-column-dropdown">
              				<?php if (!empty($PAGE->theme->settings->showlivesupport)) { ?>
              				<li>
              					<a class="CU support" href="https://portal.cornerstone.edu/student-services/info/Pages/default.aspx" target="_blank">CU Technology Support<i class="glyphicon glyphicon-new-window"></i></a>
								<a class="CU support" href="mailto:technology.support@cornerstone.edu" class="contactinfo hidden-xs">technology.support@cornerstone.edu</a>
								<a class="CU support" href="tel:616-222-1510" class="contactinfo hidden-xs">616-222-1510</a>			
							</li>   
							<li class="divider hidden-xs"></li>
							<?php } ?>         			
              				<?php if (isloggedin()) { ?>
              				<li><a class="Hangout support" href="https://support.google.com/hangouts" target="_blank">Google Hangouts Help Center<i class="glyphicon glyphicon-new-window"></i></a></li>
                            <?php if ( ($rootcategory == 'pgs') ) { ?>
                            <li><a class="Loom support" href="http://support.useloom.com" target="_blank">Loom Help Center<i class="glyphicon glyphicon-new-window"></i></a></li>
                            <?php } ?>
              				<?php if ( !empty($PAGE->theme->settings->showturnitinhelp) ) { ?>
              				<li><a class="Turnitin support" href="http://turnitin.com/en_us/support/help-center" target="_blank" title="Turnitin Help Center">Turnitin Help Center<i class="glyphicon glyphicon-new-window"></i></a></li>
              				<?php } ?>
              				<li class="divider visible-xs-block hidden-sm"></li>
              				<?php } ?>
            			</ul>
            		</div>
            		<div class="col-sm-6">
            			<ul class="multi-column-dropdown"><?php } ?>
							<li><?php echo $OUTPUT->page_doc_link(); ?></li>
              				<li><?php echo $OUTPUT->doc_link('','Moodle Documentation'); ?></li>
              				<?php if($PAGE->pagelayout != "maintenance") { ?>
              				<li class="divider hidden-xs"></li>              				         
              				<?php if ( isloggedin() && !empty($PAGE->theme->settings->showlivesupport) ) { ?>
              					<?php if ( ($rootcategory == 'pgs') ) { ?>
              						<!-- <li><a href="<?php echo $PAGE->theme->settings->orientationcourse; ?>">Moodle Orientation</a></li> -->
              					<?php } ?>
	              				<li>
	              					<a class="MDL24x7 Support" href="https://cornerstoneuniversity.zendesk.com/hc/en-us" target="mdlhelp" title="Moodle Support">Moodle Support<i class="glyphicon glyphicon-new-window"></i><br/>
	              					<span class="itemnote hidden-xs">Responses occur 24x7</span></a>
	              					<!--<a href="http://www.onlinecoursesupport.com/cornerstone/ticket/open" class="support contactinfo hidden-xs" target="mdlhelp">Open a ticket</a>-->
									<?php if ( (date('H') >= 9) && (date('H') < 21) && (date('w') >= 1) && (date('w') < 6) ) { ?><a href="tel:1-844-336-1625" class="support contactinfo hidden-xs">Call/text 844-336-1625 (9-9 M-F)</a><?php } ?>
	              				</li>
	              				<li class="divider hidden-xs"></li>
              				<?php } ?>
              				<?php } ?>
              				<li>
              					<a class="MDL Docs" href="http://www.moodle.org" target="_blank" title="About The Moodle Project - v<?php echo $CFG->release; ?>" class="img"><img src="<?php echo $OUTPUT->image_url('Moodle-Logo-RGB','theme'); ?>" alt="Moodle logo" height="25"></a>
              					<?php if($PAGE->pagelayout == "maintenance") { ?><a href="https://docs.moodle.org/dev/Moodle_3.6_release_notes" target="_blank" title="Release Notes" class="nohighlight"><?php echo $CFG->release; ?></a><?php } ?>
              				</li>
              				
            			<?php if($PAGE->pagelayout != "maintenance") { ?>
            			</ul>
            		</div>            		
            	</div><?php } ?>
			</ul>
          </li>     
		<?php if ( isloggedin() && empty($PAGE->layout_options['nonavbar']) ) { ?>  
			<?php if ( !empty($PAGE->theme->settings->showresources) ) { ?> 
          <li class="dropdown pull-right"><a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Resources<span class="caret"></span></a>
			<ul class="dropdown-menu multi-column columns-2" role="menu">
            	<div class="row">
            		<div class="col-sm-6">
            			<ul class="multi-column-dropdown">
            				<li><a href="https://hangouts.google.com/hangouts/_/cornerstone.edu/?hl=en&authuser=0" target="_blank">Start a Google Hangout</a></li>
                            <?php if ( ($rootcategory == 'pgs') ) { ?>
                            <li><a href="https://www.useloom.com" title="Install the Loom Chrome extension to easily make recordings..." target="_blank">Record your screen/webcam</a></li>
                            <?php } ?>
            				<li class="divider hidden-sm"></li>            			
            				<li><a href="https://portal.cornerstone.edu/student-services/lear/Pages/default.aspx" target="portal">Student Disability Services</a></li>
            				<li class="divider hidden-sm"></li>
            				<?php if ($rootcategory == 'pgs') { ?>          
	              				<li class="heading"><?php echo 'Online Tutoring'; ?>
	              					<ul>
	              						<li class="getatutor"><a title="Get a Tutor" href="https://www.nettutor.com/Cornerstone" target="tutor">Login to NetTutor</a></li>
	              						<li class="workshops"><a title="Signup for free workshops" href="https://www.eventbrite.com/o/cornerstone-university-pgs-2767556226" target="tutor">Signup for a Workshop</a></li>
	              					</ul>
	              				</li>
								<li class="divider hidden-sm"></li>
              				<?php } else if ( $rootcategory == 'trd' ) { ?>
              					<li><a href="https://www.cornerstone.edu/academics/learning-center/tutoring/" target="tutor" title="Tutoring Information">Tutoring</a></li>
            					<li class="divider hidden-sm"></li>
              				<?php } ?>
              				<?php if ( ($rootcategory == 'pgs') && (!empty($PAGE->theme->settings->showportallink) && !empty($PAGE->theme->settings->portalpath))) { ?>
                                <li><a href="<?php echo $PAGE->theme->settings->portalpath . '/' . $rootcategory; ?>" title="PGS Portal with Student Handbook and other resources" target="portal">PGS Student Portal</a></li>
                                <li><a href="https://moodle.cornerstone.edu/mod/resource/view.php?id=577039" title="Download the APA Template (docx)">APA Template (docx)</a></li>
              				<?php } else if ( $rootcategory != 'pgs' && (!empty($PAGE->theme->settings->showportallink) && !empty($PAGE->theme->settings->portalpath))) { ?>
              					<li><a href="<?php echo $PAGE->theme->settings->portalpath . '/' . $rootcategory; ?>" title="CU Portal" target="portal">CU Portal</a></li>
              				<?php } ?>
						</ul>
            		</div>              	
            		<div class="col-sm-6">
            			<ul class="multi-column-dropdown">
            				<li><a href="http://library.cornerstone.edu" target="library">Miller Library</a></li>
							<li><a href="http://library.cornerstone.edu/subguides" target="library">Subject Guides</a></li>
			              	<li><a href="http://library.cornerstone.edu/az.php" target="library">Databases</a></li>
			              	<li><a href="http://eaglelink.cornerstone.edu/" target="library">Catalog</a></li>
			              	<li><a href="http://askalibrarian.cornerstone.edu/" target="library">FAQ</a></li>              
			              	<li class="divider"></li>
			              	<li>
			              		<form id="librarysearch" class="navbar-form navbar-left" role="search" method='GET' action='https://cornerstone.summon.serialssolutions.com/search' target='library'>
			          			<div class="form-group">
			            			<input id="summonsearchtext" type="text" name="s.q" class="form-control" placeholder="Search the library...">
			            			<input type='hidden' name='spellcheck' value='true'>
			            			<input id='summonparameters1' type='hidden' name='id' value='#seb1c77c083720130584b683935c2bb54'>
			            			<input id='summonparameters2' type='hidden' name='s.fvf[]' value='ContentType,Newspaper Article,t'>
			            			<input id='summonparameters3' type='hidden' name='s.fvf[]' value='ContentType,Book Review,t'>
			            			<input id='summonparameters4' type='hidden' name='keep_r' value='true'>
			            			<input type='submit' value='Search' title='Search results open in a new window'>
			          			</div>
			          			<div class='whatissummon'><a href='https://cornerstone.summon.serialssolutions.com/advanced' target='library'>Advanced Search</a></div>
			        	  	</form></li>              				
            			</ul>
            		</div>          		
            	</div>
			</ul>          
          </li>
          <?php } ?>
		  <li class="dropdown pull-right"><a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Courses<span class="caret"></span></a>
			<ul id="cu_coursemenu" class="dropdown-menu wide-menu" role="menu">  
              	<li><a href="<?php echo $CFG->wwwroot; ?>/my" title="Dashboard"><i class="fa fa-graduation-cap" aria-hidden="false" aria-label="Go to My Moodle"></i>My Moodle</a></li>
              	<li class="divider"></li>
              	<?php echo $OUTPUT->custom_mycourses_menu(); ?>
              	<li><a href="<?php echo $CFG->wwwroot; ?>/course/index.php">All CU Courses</a></li>  
              	<li>
              		<form id="cu-nav-coursesearch" action="<?php echo $CFG->wwwroot; ?>/course/search.php" method="get" class="navbar-form navbar-left" role="search">
          				<div class="form-group"><input type="text" id="coursesearchbox" name="search" class="form-control" placeholder="Search for a course..."></div>
          				<input type="submit" value="Search" class="btn btn-default">
        	  		</form>
        	  	</li>              				
			</ul>          
          </li> 
          <li id="cu-navbar-dashboard-link" class="pull-right"><a href="<?php echo $CFG->wwwroot; ?>/my" title="Dashboard"><i class="fa fa-graduation-cap" aria-hidden="false" aria-label="Go to My Moodle"></i>My Moodle</a></li>           
		<?php } ?>          
        </ul>       
      </div><!-- /.navbar-header -->
    </div><!-- /#cu-sitenav.container-fluid -->
    <div class="container-fluid <?php echo $cudockclass; ?>" id="cu-dock">
    	<div id="dock"></div>
    	<ul id="cu-courseinfo-block" class="nav navbar-nav">
    	    <?php if($COURSE->id != 1) { ?>
        	<li class="dropdown"><a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><?php echo $cupageheading; ?></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="<?php echo $CFG->wwwroot; ?>/course/view.php?id=<?php echo $COURSE->id; ?>&section=0">Course Overview</a></li>
              <li class="divider hidden-sm"></li>                    
              <li><a href="<?php echo $CFG->wwwroot; ?>/user/index.php?id=<?php echo $COURSE->id; ?>">Participants</a></li> 
              <li><a href="<?php echo $CFG->wwwroot; ?>/blocks/quickmail/email.php?courseid=<?php echo $COURSE->id; ?>">Send Quickmail</a></li>
              <li><a href="https://hangouts.google.com/hangouts/_/cornerstone.edu/?hl=en&authuser=0" target="_blank">Start a Google Hangout</a></li>
              <li class="divider hidden-sm"></li> 
              <?php if ( has_capability('local/cugrader:grade', $context) ) { ?>             
                <li><a href="<?php echo $CFG->wwwroot; ?>/local/cugrader/view.php?courseid=<?php echo $COURSE->id; ?>">CU Grader</a></li>
              <?php } ?>
              <?php if (has_capability('gradereport/grader:view', $context)) { ?>
              	<li><a href="<?php echo $CFG->wwwroot; ?>/grade/report/grader/index.php?id=<?php echo $COURSE->id; ?>">Gradebook</a></li>
              <?php } else { ?>
              	<li><a href="<?php echo $CFG->wwwroot; ?>/grade/report/user/index.php?id=<?php echo $COURSE->id; ?>">Grade Report</a></li>
              	<?php if ( has_capability('local/cugrader:view', $context) ) { ?>             
                	<li><a href="<?php echo $CFG->wwwroot; ?>/local/cugrader/view.php?courseid=<?php echo $COURSE->id; ?>">View Graded Activities</a></li>
              	<?php } ?>
              <?php } ?>
              <?php if (has_capability('moodle/grade:manage', $context) && $has_ilp_access && count($course_shortname)>1 ) { ?>
                <?php if ($rootcategory == 'sem' || $rootcategory == 'trd' || $rootcategory == 'grd') { ?>
				      <li class="ilp ilp_midterm"><a href="<?php echo $CFG->wwwroot; ?>/blocks/intelligent_learning/view.php?controller=midtermgrades&action=edit&courseid=<?php echo $COURSE->id; ?>">Submit <?php echo get_string('midtermgrades', 'block_intelligent_learning'); ?></a></li>
                      <li class="ilp ilp_final"><a href="<?php echo $CFG->wwwroot; ?>/blocks/intelligent_learning/view.php?controller=finalgrades&action=edit&courseid=<?php echo $COURSE->id; ?>">Submit <?php echo get_string('finalgrades', 'block_intelligent_learning'); ?></a></li>
              	<?php } else if ( $rootcategory == 'pgs' ) { ?>
                      <li class="ilp ilp_final"><a href="<?php echo $CFG->wwwroot; ?>/blocks/intelligent_learning/view.php?controller=finalgrades&action=edit&courseid=<?php echo $COURSE->id; ?>">Submit <?php echo get_string('finalgrades', 'block_intelligent_learning'); ?></a></li>
              	<?php } ?>
              <?php } ?>
		<?php if (has_capability('gradereport/grader:view', $context) && $rootcategory == 'pgs' && count($course_shortname)>1 ) { ?>
			<li class="divider hidden-sm"></li>
			<li><a href="https://docs.google.com/forms/d/1sRNNK5CCIffvk1icHkcbGoeBR6XGORU219f3S-J2JSk/viewform?entry.1250342568=<?php echo $USER->firstname . '+' . $USER->lastname; ?>&entry.1525128316=<?php echo $USER->email; ?>&entry.1292200380=<?php echo $USER->username; ?>&entry.1308361914=<?php echo $COURSE->shortname; ?>&entry.999549132=<?php echo substr($COURSE->shortname, 7, 7); ?>" target="_blank">Faculty End-of-Course Survey</a></li>
			<li class="divider hidden-sm"></li>
       		<li class="studentreferal"><a title="Refer a PGS student for additional academic support" href="https://docs.google.com/forms/d/e/1FAIpQLSfeAeKSF9WR_LRBc8ro-LVSPsveWi5Wjgt8IS4MukRQWx-uqA/formResponse?entry.128281232=<?php echo mb_substr($course_shortname[1],0,7); ?>&entry.1150926614=<?php echo $course_shortname[0]; ?>" target="referalform">Student Support Referral</a></li>
		<?php } ?>  
              <?php if (( $curole == 'student' ) && (!has_capability('gradereport/grader:view', $context))) { ?>
        		<?php if ( $rootcategory == 'pgs' ) { ?>  
	            	<li class="divider hidden-sm"></li>        
	              	<li class="getatutor"><a title="Get a Tutor" href="https://www.nettutor.com/Cornerstone" target="tutor">Online Tutoring</a></li>
              	<?php } else if ( $rootcategory == 'trd' ) { ?>
	              	<!-- <li class="divider hidden-sm"></li>        
	              	<li><a href="https://www.cornerstone.edu/academics/learning-center/tutoring/" target="tutor" title="Tutoring Information">Tutoring</a></li> -->
              	<?php } ?>    
              <?php } ?>
            </ul>
          </li>   
          <?php } ?>
        </ul>
    	<ul id="cu-quicklinks" class="nav navbar-nav navbar-right">
          <?php if (!empty($PAGE->theme->settings->showportallink)) {
          	echo '<li><a href="'.$PAGE->theme->settings->portalpath.'" target="portal"  title="CU Portal"><i class="fa fa-2x fa-university" aria-hidden="false" aria-label="Go to the University portal"></i><span class="hidden">CU Portal</span></a></li>';
          } ?>
          <?php if (!empty($PAGE->theme->settings->showemaillink)) {
          	echo '<li><a href="'.$PAGE->theme->settings->emailpath.'" target="_blank"  title="CU Email"><i class="fa fa-2x fa-envelope" aria-hidden="false" aria-label="Go to the University email"></i><span class="hidden">CU Email</span></a></li>';
          } ?>
          <?php // if (isloggedin()) { ?>
          	<!-- <li><a href="<?php echo $CFG->wwwroot; ?>/login/logout.php?sesskey=<?php echo $USER->sesskey; ?>" title="Logout"><i class="fa fa-2x fa-sign-out" aria-hidden="false" aria-label="Log out"></i><span class="hidden">Log out</span></a></li> -->
          <?php //} ?>
          <?php if (!isloggedin() && $PAGE->pagelayout != "login") { ?>
          <form action="<?php echo $CFG->wwwroot; ?>/login" method="post" class="pull-right" role="form">
         		<button id="cu-login-button" type="submit" class="btn btn-default">Log in</button>
		  </form>
          <?php } ?> 
    	</ul>
	<div id="cu-mdlsearch" class="navbar-right">
		<?php echo $OUTPUT->search_box(); ?>
	</div>
    </div><!-- /#cu-dock -->
</nav>
