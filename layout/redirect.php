<?php

echo $OUTPUT->doctype(); ?>
<html <?php echo $OUTPUT->htmlattributes() ?>>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<title><?php echo $PAGE->title ?></title>
	
	<link rel="shortcut icon" href="<?php echo $OUTPUT->favicon(); ?>" />
	
	<?php echo $OUTPUT->standard_head_html() ?>
	
	<!-- Load the BRANDON GROTESQUE font among others from Adobe's Typekit -- Requires permissions by domain -->
	<script src="https://use.typekit.net/ndq0ppg.js"></script>
	<script>try{Typekit.load({ async: true });}catch(e){}</script>
</head>

<body id="<?php echo $PAGE->bodyid; ?>" class="site <?php echo $PAGE->bodyclasses; ?>">

<!-- NAVBAR -->
<?php 
	echo $OUTPUT->standard_top_of_body_html();
	require_once(dirname(__FILE__).'/navbar.php'); 
?>

<!-- HEADER -->
<?php if ($PAGE->heading || (empty($PAGE->layout_options['nonavbar']) && $PAGE->hasnavbar())) {
	require_once(dirname(__FILE__).'/header.php');
} ?>

<section id="page" class="site-content">	
	<main id="page-content">
		<div id="region-main-box">
			<div id="region-post-box">
				<div id="region-main-wrap">
					<div id="region-main">
						<div class="region-content">
							<?php echo $OUTPUT->main_content(); ?>
						</div>
					</div> <!-- CLOSE region-main -->
				</div> <!-- CLOSE region-main-wrap -->
			</div> <!-- CLOSE region-post-box -->
		</div> <!-- CLOSE region-main-box -->
	</main>
</section>
	
<!-- PAGE FOOTER -->
<?php 
	require_once(dirname(__FILE__).'/footer.php');
	require_once(dirname(__FILE__).'/debug.php');
	echo $OUTPUT->standard_end_of_body_html() 
?>
</body>
</html>