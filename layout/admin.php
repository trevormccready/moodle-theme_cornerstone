<?php
$bodyclasses = array();
$usingie = strpos($PAGE->bodyclasses, 'ie ie');
if (($usingie !== false)) {
	header('Location: '.$CFG->wwwroot.'/login/logout.php?sesskey='.$USER->sesskey);
}
if ( !empty($CFG->maintenance_enabled)) {
	$bodyclasses[] = 'maintenance_mode';
} else if ( isset($CFG->maintenance_later) ) {
	$bodyclasses[] = 'maintenance_warn';
}
$HTML = theme_cornerstone_get_settings($OUTPUT, $PAGE);
$bodyclasses[] = $rootcategory = theme_cornerstone_get_root_category($PAGE);
$bodyclasses[] = $curole = theme_cornerstone_get_course_role($PAGE);

if ($HTML->cufullscreenpreference == 1) {
	$bodyclasses[] = 'fullscreen';
}
$showsidepre = '';
$showsidepost = '';
$hassidepre = $PAGE->blocks->region_has_content('side-pre', $OUTPUT);
$hassidepost = $PAGE->blocks->region_has_content('side-post', $OUTPUT);
if( $hassidepre ) {
	if ( empty($PAGE->layout_options['nonavbar']) && $PAGE->blocks->region_completely_docked('side-pre', $OUTPUT) ) {
		$showsidepre = false;
	} else {
		$showsidepre = true;
	}
}
if( $hassidepost ) {
	if ( empty($PAGE->layout_options['nonavbar']) && $PAGE->blocks->region_completely_docked('side-post', $OUTPUT) ) {
		$showsidepost = false;
	} else {
		$showsidepost = true;
	}
}

if ($showsidepre && !$showsidepost) {
	$bodyclasses[] = 'side-pre-only';
	$bodyclasses[] = 'side-pre';
} else if (!$showsidepre && $showsidepost) {
	$bodyclasses[] = 'side-post-only';
	$bodyclasses[] = 'side-post';
} else if (!$showsidepre && !$showsidepost) {
	$bodyclasses[] = 'content-only';
} else {
	$bodyclasses[] = 'side-pre';
	$bodyclasses[] = 'side-post';
}

echo $OUTPUT->doctype(); ?>
<html <?php echo $OUTPUT->htmlattributes() ?>>
<head>
	<!-- Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-P9GR5S8');</script>
	<!-- End Google Tag Manager -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<title><?php echo $PAGE->title ?></title>
	
	<link rel="shortcut icon" href="<?php echo $OUTPUT->favicon(); ?>" />
	
	<?php echo $OUTPUT->standard_head_html() ?>
	
	<!-- Load the BRANDON GROTESQUE font among others from Adobe's Typekit -- Requires permissions by domain -->
	<script src="https://use.typekit.net/ndq0ppg.js"></script>
	<script>try{Typekit.load({ async: true });}catch(e){}</script>
</head>

<body id="<?php echo $PAGE->bodyid; ?>" onload="loadGroupPage();" class="site <?php echo $PAGE->bodyclasses.' '.join(' ',$bodyclasses); ?>">
<?php include_once(dirname(__FILE__).'/analyticstracking.php'); ?>
<?php echo $OUTPUT->standard_top_of_body_html() ?>

<!-- NAVBAR -->
<?php if (empty($PAGE->layout_options['nonavbar'])) {
	require_once(dirname(__FILE__).'/navbar.php');
} ?>

<!-- HEADER -->
<?php if ($PAGE->heading || (empty($PAGE->layout_options['nonavbar']) && $PAGE->has_navbar())) {
	require_once(dirname(__FILE__).'/header.php');
} ?>

<section id="page" class="site-content">
	<?php 
		if ($hassidepre || $hassidepost) {
			$blockregionclass="col-sm-3";
			$maincontentclass="col-sm-9";
	?>
	<aside id="cu-block-region" class="region-pre-container region-post-container <?php echo $blockregionclass; ?>">
		<?php if ($hassidepre) { ?>
			<div id="region-pre" class="block-region">
				<div class="region-content"><?php echo $OUTPUT->blocks_for_region('side-pre') ?></div>
			</div>
		<?php } ?>
		<?php if ($hassidepost) { ?>
			<div id="region-post" class="block-region">
				<div class="region-content"><?php echo $OUTPUT->blocks_for_region('side-post') ?></div>
			</div>
		<?php } ?>
	</aside>
	<?php } else {
		$maincontentclass="col-sm-12";
	}
	?>
	
	<main id="page-content" class="<?php echo $maincontentclass; ?>">
		<div id="region-main-wrap">
			<div id="region-main">
				<div class="region-content">
					<?php echo $OUTPUT->main_content(); ?>
				</div>
			</div> <!-- CLOSE region-main -->
		</div> <!-- CLOSE region-main-wrap -->
	</main>
</section>
	
<!-- PAGE FOOTER -->
<?php if (empty($PAGE->layout_options['nofooter'])) {
	require_once(dirname(__FILE__).'/footer.php');
} ?>

<!--  DEBUG INFORMATION -->
<?php require_once(dirname(__FILE__).'/debug.php'); ?>

<?php echo $OUTPUT->standard_end_of_body_html() ?>
<script type="text/javascript">
function loadGroupPage() {
    var group_page = document.getElementById("page-group-group");
    var grouping_page = document.getElementById("page-group-grouping");
    if (group_page || grouping_page) {
       var group_idnumber_field = document.getElementById("id_idnumber"); 
        if ( group_idnumber_field.value.length > 0 && (group_idnumber_field.disabled == true || group_idnumber_field.readOnly == true)) {
            document.getElementById("id_name").disabled = true;
        } 
    }
}
</script>
</body>
</html>
