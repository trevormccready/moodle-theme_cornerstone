<footer class="<?php echo $PAGE->pagelayout; ?> clearfix hidden-print">
	<div id="page-footer" class="wrapper">
	<?php if (empty($PAGE->layout_options['nofooter'])) { ?>
	<div id="cu-footer-content" class="container-fluid">
		<div id="cu-footer-logo" class="col-sm-3"><a href="https://www.cornerstone.edu/" target="_blank" title="Cornerstone University Website Homepage">
			<img src="<?php echo $OUTPUT->image_url('footerlogo','theme'); ?>" title="Cornerstone Logo"></a>
		</div>
		<?php if (!empty($PAGE->theme->settings->showlivesupport)) { ?>
		<div id="cu-footer-support" class="col-sm-3">
			<h2>Support</h2>
			<ul>
				<?php if (isloggedin()) { ?>
				<li class="helplink"><a href="https://cornerstoneuniversity.zendesk.com/hc/en-us" target="_blank">Moodle Support (24x7)</a> <i class="glyphicon glyphicon-new-window"></i></li>
				<?php } ?>
				<li class="helplink">
					<a href="https://portal.cornerstone.edu/student-services/info/Pages/default.aspx" target="_blank">CU Technology Support<i class="glyphicon glyphicon-new-window"></i></a><br/>
					<?php if (!isloggedin()) { ?>
					<a href="mailto:technology.support@cornerstone.edu" class="contactinfo">technology.support@cornerstone.edu</a>
					<a href="tel:616-222-1510" class="contactinfo">616-222-1510</a><br/>
					<?php } ?>			
				</li>
			</ul>
		</div>
		<?php } ?>
		<div id="cu-footer-resources" class="col-sm-3">
			<h2>Documentation</h2>
			<ul>
				<li class="helplink"><a href="https://support.google.com/hangouts/?hl=en" target="_blank" title="Google Hangouts Help Center">Google Hangouts Help Center<i class="glyphicon glyphicon-new-window"></i></a></li>
				<li id="cu-footer-moodledocs" class="cu-navbar-item"><a href="https://docs.moodle.org/29/en/Main_page" target="_blank" title="Moodle Community Documentation">Moodle Documentation <i class="glyphicon glyphicon-new-window"></i></a></li>
				<?php if ( !empty($PAGE->theme->settings->showturnitinhelp) ) { ?>
				<li class="helplink"><a href="http://turnitin.com/en_us/support/help-center" target="_blank" title="Turnitin Help Center">Turnitin Help Center <i class="glyphicon glyphicon-new-window"></i></a></li>
				<?php } ?>
				</ul>
		</div>
		<div id="cu-footer-quicklinks" class="col-sm-3">
			<h2>Quick Links</h2>
			<ul>
				<?php if ( !isloggedin() ) { ?>
			  		<li><a href="<?php echo $CFG->wwwroot; ?>/abts" title="Login to ABTS' Moodle"><i class="fa fa-graduation-cap" aria-hidden="false" aria-label="Login to ABTS' Moodle"></i>ABTS Moodle Login</a>
			  	<?php } ?>	
              <?php if (!empty($HTML->emaillink)) { ?><li><?php echo $HTML->emaillink; ?></li><?php } ?>   
              <?php if (!empty($HTML->portallink)) { ?><li><?php echo $HTML->portallink; ?></li><?php } ?>
              <?php if (!empty($PAGE->theme->settings->website)) { ?>
				<li class="quicklink"><a href="<?php echo $PAGE->theme->settings->website; ?>" target="_blank"><i class="fa fa-globe" aria-hidden="false" aria-label="Go to main website"></i><?php echo $PAGE->theme->settings->orgname; ?> Website</a></li>
			  <?php } ?>
			</ul>
		</div>
	</div>
	<?php } ?>
	<div id="cu-footer-copyright" class="container-fluid">
		<div id="cu-copyright" class="pull-left">Copyright &copy; <?php echo date("Y") ?> <a href="https://www.cornerstone.edu/" target="_blank" title="Visit www.cornerstone.edu">Cornerstone University</a>. All rights reserved.</div>
		<?php if (is_siteadmin()) { ?>
		<div id="cu-footer-admincontrols" class="pull-right">
			<span class="glyphicon glyphicon-flash" style="color: yellow;" data-toggle="collapse" data-target="#cu-debug-info" title="Site Administrator"></span>
		</div>
		<?php } ?>
		<?php if (empty($PAGE->layout_options['nofooter'])) { ?>
		<div id="cu-footer-quicklinkicons" class="pull-right">
		  <?php if (!empty($PAGE->theme->settings->website)) { ?>
			<a href="<?php echo $PAGE->theme->settings->website; ?>" target="_blank" title="Visit the main website"><i class="fa fa-2x fa-globe" aria-hidden="false" aria-label="Go to main website"></i></a>
		  <?php } ?>
		  <?php if (!empty($PAGE->theme->settings->showportallink)) {
          	echo '<a href="'.$PAGE->theme->settings->portalpath.'" target="_blank"  title="CU Portal"><i class="fa fa-2x fa-university" aria-hidden="false" aria-label="Go to the University portal"></i><span class="hidden">CU Portal</span></a>';
          } ?>
          <?php if (!empty($PAGE->theme->settings->showemaillink)) {
          	echo '<a href="'.$PAGE->theme->settings->emailpath.'" target="_blank"  title="CU Email"><i class="fa fa-2x fa-envelope" aria-hidden="false" aria-label="Go to the University email"></i><span class="hidden">CU Email</span></a>';
          } ?>
          <?php if (isloggedin()) { ?>
          	<a href="<?php echo $CFG->wwwroot; ?>/login/logout.php?sesskey=<?php echo $USER->sesskey; ?>" title="Logout"><i class="fa fa-2x fa-sign-out" aria-hidden="false" aria-label="Log out"></i><span class="hidden">Log out</span></a>
          <?php } ?>
         </div>
		<?php } ?>
	</div>
</footer> 
