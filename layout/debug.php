<div id="cu-debug-info" class="container collapse row">
	<hr/>
    <div><a href="<?php echo $CFG->wwwroot; ?>?theme=rock">New Theme Preview</a></div>
	<div>--- DEBUGGING INFORMATION ---</div>
	<!-- USER DETAILS -->
	<div>
		USER_AGENT: <?php echo $_SERVER['HTTP_USER_AGENT']; ?>
	</div>
	<!-- MOODLE VERSION -->
	<div>
		Moodle <?php echo $CFG->release; ?>.
		Version: <?php echo $CFG->version; ?>.
		<a href="https://docs.moodle.org/dev/Releases" target="_blank" title="View official Moodle release notes">Release notes</a>.
	</div>
	<!-- THEME INFORMATION -->
	<div>
		Theme: <?php echo $PAGE->theme->name; ?>. 
		Version: <?php echo $PAGE->theme->settings->version; ?>. 
		Parents: 
			<?php if (empty($PAGE->theme->parents)) {
				echo "None";
			} else {
				print_r( array_values( $PAGE->theme->parents )); 
			} ?>.
		<br />
		Page Layout: <?php echo $PAGE->pagelayout; ?>.
		<br />
		Stylesheets: 
			<?php if (empty($PAGE->theme->sheets)) {
				echo "None";
			} else {
				print_r( array_values( $PAGE->theme->sheets )); 
			}?>.
		<br/>
		Javascript: 
			<?php if (empty($PAGE->theme->javascripts) AND empty($PAGE->theme->javascripts_footer)) {
				echo "None";
			} else {
				if (!empty($PAGE->theme->javascripts)) {
					echo "HEAD: ";
					print_r( array_values( $PAGE->theme->javascripts ));
				}
				if (!empty($PAGE->theme->javascripts_footer )) {
					echo "FOOTER: ";
					print_r( array_values( $PAGE->theme->javascripts_footer ));
				}		
			}?>.
	</div>
	<div>
		Timezone DB version: <?php echo timezone_version_get(); ?>.
	</div>
    <div>
        <?php echo strtotime('today');?>
    </div>
	<?php echo $OUTPUT->standard_footer_html();	?>
</div>
