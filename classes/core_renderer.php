<?php
class theme_cornerstone_core_renderer extends core_renderer {
	
	public function doc_link($path, $text = '', $forcepopup = false) {
		global $CFG;
	
		$icon = $this->pix_icon('docs', $text, 'moodle', array('class'=>'iconhelp icon-pre'));
	
		$url = new moodle_url(get_docs_url($path));
	
		$attributes = array('href'=>$url);
		if (!empty($CFG->doctonewwindow)) {
			$attributes['target'] = '_blank';
			$attributes['class'] = 'MDL Docs';
		} elseif ($forcepopup) {
			$attributes['class'] = 'MDL Docs helplinkpopup';
		} else {
			$attributes['class'] = 'MDL Docs';
		}
		$newwindowicon = '<i class="glyphicon glyphicon-new-window"></i>';
	
		return html_writer::tag('a', $icon.$text.$newwindowicon, $attributes);
	}
	
	/**
	 * Outputs the courses menu
	 * @return custommenu object
	 */
	public function custom_mycourses_menu($custommenuitems = '') {
		// The custom menu is always shown, even if no menu items
		// are configured in the global theme settings page.
		global $CFG;
	
		if (!empty($CFG->custommenuitems)) {
			$custommenuitems .= $CFG->custommenuitems;
		}
		$custommenu = new custom_menu($custommenuitems, current_language());
		return $this->render_custom_menu($custommenu);
	}
	
	protected function render_custom_menu(custom_menu $menu) {
	global $CFG, $PAGE, $SITE, $USER;
		require_once($CFG->dirroot.'/course/lib.php');
		$content = '';
		$numhidden = 0;
		
		// Generate the MY COURSES custom menu
		if (isloggedin() && !isguestuser()) {
			if ($mycourses = enrol_get_my_courses(NULL, 'visible DESC, sortorder ASC, startdate ASC, shortname ASC')) {
				foreach ($mycourses as $mycourse) {
					if($mycourse->visible) {
						$menu->add($mycourse->shortname, new moodle_url('/course/view.php', array('id' => $mycourse->id),$mycourse->fullname));
					} else if (has_capability('moodle/course:viewhiddencourses', context_course::instance($mycourse->id))) {
						$numhidden += 1;
					}
				}
			} else {
				$menu->add('No enrolled/visible courses', null, null, 'You are either not enrolled in any visible courses');
			}
			$menu->add('divider');
		}
		foreach ($menu->get_children() as $item) {
			$content .= $this->render_custom_menu_item($item, 1);
		}
		if ($numhidden > 0) {
			$branch = $menu->add('divider');
			$branch->add('You have '.$numhidden.' hidden course(s)', null, null, 'Hidden courses can be viewed from the Dashboard');
			$branch->add('All My Courses', new moodle_url('/my/index.php', array('mynumber' => '-2'),'View All My Courses'));
			$branch->add('divider');
			foreach ($branch->get_children() as $item) {
				$content .= $this->render_custom_menu_item($item, 1);
			} 
		}
		return $content;
	}
	
	public function render_custom_menu_item(custom_menu_item $menunode, $level = 0 ) {
		static $submenucount = 0;
	
		if ($menunode->has_children()) {
	
			if ($level == 1) {
				$dropdowntype = 'dropdown';
			} else {
				$dropdowntype = 'dropdown-submenu';
			}
	
			$content = html_writer::start_tag('li', array('class' => $dropdowntype));
			// If the child has menus render it as a sub menu.
			$submenucount++;
			if ($menunode->get_url() !== null) {
				$url = $menunode->get_url();
			} else {
				$url = '#cm_submenu_'.$submenucount;
			}
			$linkattributes = array(
					'href' => $url,
					'class' => 'dropdown-toggle',
					'data-toggle' => 'dropdown',
					'title' => $menunode->get_title(),
			);
			$content .= html_writer::start_tag('a', $linkattributes);
			$content .= $menunode->get_text();
			if ($level == 1) {
				$content .= '<b class="caret"></b>';
			}
			$content .= '</a>';
			$content .= '<ul class="dropdown-menu">';
			foreach ($menunode->get_children() as $menunode) {
				$content .= $this->render_custom_menu_item($menunode, 0);
			}
			$content .= '</ul>';
		} else {
			if ($menunode->get_text() == 'divider') {
				$content = html_writer::end_tag('li');
				$content .= html_writer::tag('li',null, array('class' => 'divider'));
			} else {
				// The node doesn't have children so produce a final menuitem.
				if ($menunode->get_url() !== null) {
					$content = '<li>';
					$url = $menunode->get_url();
					$content .= html_writer::link($url, $menunode->get_text(), array('title' => $menunode->get_title()));
				} else {
					$linote = '<span class="note">'.$menunode->get_text().'</span>';
					$content = html_writer::tag('li', $linote, array('class' => 'nolink', 'title' => $menunode->get_title()));
				}
			}
		}
		return $content;
	}
	
	/**
	 * Renders tabtree
	 *
	 * @param tabtree $tabtree
	 * @return string
	 */
	public function render_tabtree(\tabtree $tabtree) {
		if (empty($tabtree->subtree)) {
			return false;
		}
		$firstrow = $secondrow = '';
		foreach ($tabtree->subtree as $tab) {
			$firstrow .= $this->render($tab);
			if (($tab->selected || $tab->activated) && !empty($tab->subtree) && $tab->subtree !== array()) {
				$secondrow = $this->tabtree($tab->subtree);
			}
		}
		return html_writer::tag('ul', $firstrow, array('class' => 'nav nav-tabs')) . $secondrow;
	}
	/**
	 * Renders tabobject (part of tabtree)
	 *
	 * This function is called from {@link core_renderer::render_tabtree()}
	 * and also it calls itself when printing the $tabobject subtree recursively.
	 *
	 * @param tabobject $tab
	 * @return string HTML fragment
	 */
	public function render_tabobject(\tabobject $tab) {
		if ($tab->selected or $tab->activated) {
			return html_writer::tag('li', html_writer::tag('a', $tab->text), array('class' => 'active'));
		} else if ($tab->inactive) {
			return html_writer::tag('li', html_writer::tag('a', $tab->text), array('class' => 'disabled'));
		} else {
			if (!($tab->link instanceof moodle_url)) {
				// Backward compartibility when link was passed as quoted string.
				$link = "<a href=\"$tab->link\" title=\"$tab->title\">$tab->text</a>";
			} else {
				$link = html_writer::link($tab->link, $tab->text, array('title' => $tab->title));
			}
			return html_writer::tag('li', $link);
		}
	}
	
	/**
	 * Produces a header for a block.
	 *
	 * @param block_contents $bc.
	 * @return string.
	 */
	protected function block_header(block_contents $bc) {
		$title = '';
		if ($bc->title) {
			$attributes = array();
			if ($bc->blockinstanceid) {
				$attributes['id'] = 'instance-'.$bc->blockinstanceid.'-header';
			}
			static $icons = array(
					'activity_modules' => 'puzzle-piece',
					'admin_bookmarks' => 'bookmark',
					'adminblock' => 'th-large',
					'blog_menu' => 'book',
					'blog_tags' => 'tags',
					'book_toc' => 'book',
					'calendar_month' => 'calendar',
					'calendar_upcoming' => 'calendar',
					'comments' => 'comments',
					'community' => 'globe',
					'completionstatus' => 'tachometer',
					'course_badges' => 'trophy',
					'course_list' => 'desktop',
					'feedback' => 'thumbs-o-up',
					'flickr' => 'flickr',
					'glossary_random' => 'lightbulb-o',
					'html' => 'list-alt',
					'iconic_html' => '', // It decides.
					'login' => 'user',
					'messages' => 'envelope',
					'mentees' => 'tags',
					'navigation' => 'sitemap',
					'news_items' => 'bullhorn',
					'myprofile' => 'user',
					'online_users' => 'users',
					'participants' => 'users',
					'private_files' => 'folder-o',
					'quiz_navblock' => 'code-fork',
					'quiz_results' => 'bar-chart',
					'recent_activity' => 'clock-o',
					'rss_client' => 'rss',
					'search_forums' => 'comments-o',
					'section_links' => 'bookmark',
					'selfcompletion' => 'tachometer',
					'settings' => 'cogs',
					'style_guide' => 'paint-brush',
					'tags' => 'tags',
					'theme_selector' => 'paint-brush',
					'twitter_search' => 'twitter',
					'youtube' => 'youtube'
			);
			if (array_key_exists($bc->attributes['data-block'], $icons)) {
				$theicon = $icons[$bc->attributes['data-block']];
			} else {
				$theicon = 'reorder';
			}
			$title = html_writer::tag('h2', $bc->title, $attributes);
			if (!empty($theicon)) {
				$title = $this->getfontawesomemarkup($theicon).$title;
			}
		}
		$blockid = null;
		if (isset($bc->attributes['id'])) {
			$blockid = $bc->attributes['id'];
		}
		$controlshtml = $this->block_controls($bc->controls, $blockid);
		$output = '';
		if ($title || $controlshtml) {
			$output .= html_writer::tag('div', html_writer::tag('div',
					html_writer::tag('div', '', array('class' => 'block_action')).$title.$controlshtml, array('class' => 'title')),
					array('class' => 'header'));
		}
		return $output;
	}
	
	/*
	 * This code replaces icons in with
	 * FontAwesome variants where available.
	 */
	/* public function render_pix_icon(pix_icon $icon) {
		static $icons = array(
				'add' => 'plus',
				'book' => 'book',
				'chapter' => 'file',
				'docs' => 'question-circle',
				'generate' => 'gift',
				'i/marker' => 'lightbulb-o',
				'i/delete' => 'times-circle',
				'i/dragdrop' => 'arrows',
				'i/loading' => 'refresh fa-spin fa-2x',
				'i/loading_small' => 'refresh fa-spin',
				'i/backup' => 'cloud-download',
				'i/checkpermissions' => 'user',
				'i/competencies' => 'wifi fa-flip-vertical',
				'i/edit' => 'pencil',
				'i/enrolusers' => 'user-plus',
				'i/filter' => 'filter',
				'i/grades' => 'table',
				'i/group' => 'group',
				'i/groupn' => 'user',
				'i/groupv' => 'user-plus',
				'i/groups' => 'user-secret',
				'i/hide' => 'eye',
				'i/import' => 'upload',
				'i/move_2d' => 'arrows',
				'i/navigationitem' => 'file',
				'i/outcomes' => 'magic',
				'i/preview' => 'search',
				'i/publish' => 'globe',
				'i/reload' => 'refresh',
				'i/report' => 'list-alt',
				'i/restore' => 'cloud-upload',
				'i/return' => 'repeat',
				'i/roles' => 'user',
				'i/cohort' => 'users',
				'i/scales' => 'signal',
				'i/settings' => 'cogs',
				'i/show' => 'eye-slash',
				'i/switchrole' => 'random',
				'i/user' => 'user',
				'i/users' => 'user',
				't/right' => 'arrow-right',
				't/left' => 'arrow-left',
				't/edit_menu' => 'cogs',
				'i/withsubcat' => 'indent',
				'i/permissions' => 'key',
				'i/assignroles' => 'lock',
				't/assignroles' => 'lock',
				't/block_to_dock' => 'caret-square-o-left',
				't/cohort' => 'users',
				't/copy' => 'copy',
				't/delete' => 'times-circle',
				't/down' => 'arrow-down',
				't/edit' => 'cog',
				't/editstring' => 'pencil-square-o',
				't/grades' => 'th-list',
				't/hide' => 'eye',
				't/preview' => 'search',
				't/show' => 'eye-slash',
				't/sort' => 'sort',
				't/sort_asc' => 'sort-asc',
				't/sort_desc' => 'sort-desc',
				't/up' => 'arrow-up'
		);
		if (array_key_exists($icon->pix, $icons)) {
			$pix = $icons[$icon->pix];
			if (empty($icon->attributes['alt'])) {
				return '<i class="fa fa-'.$pix.' icon" aria-hidden="true">'.parent::render_pix_icon($icon).'</i>';
			} else {
				$alt = $icon->attributes['alt'];
				return '<i class="fa fa-'.$pix.' icon" title="'.$alt.'" aria-hidden="true">'.parent::render_pix_icon($icon).'</i>';
			}
		} else {
			return parent::render_pix_icon($icon);
		}
	} */
	
	private function getfontawesomemarkup($theicon, $classes = array(), $attributes = array(), $content = '') {
		$classes[] = 'fa fa-'.$theicon;
		$attributes['aria-hidden'] = 'true';
		$attributes['class'] = implode(' ', $classes);
		return html_writer::tag('span', $content, $attributes);
	}
	
}
