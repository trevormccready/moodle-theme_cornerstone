var $j = jQuery.noConflict();

/* DOCK SETTINGS */
function customise_dock_for_theme() {
    var dock = M.core_dock;
    dock.cfg.position = 'top';
    dock.cfg.orientation = 'horizontal';
    dock.on('dock:resizepanelcomplete', theme_cornerstone_handle_dock_resize);
    dock.on('dock:itemadded', theme_cornerstone_handle_dockedblocks);
}

function theme_cornerstone_handle_dockedblocks(e) {
	
}

function theme_cornerstone_handle_dock_resize() {
    // Check its visible no point doing anything if its not.
    if ( $j( '#dockeditempanel' ).is(':hidden') ) {
        return;
    }
    var screenheight = $j( window ).height();
    var scrolltop = $j( '.dockeditempanel_bd' ).scrollTop();
    // Reset the height of the panel so that we can accurately measure it
    $j( '.dockeditempanel_bd' ).height('auto');
    // Remove the oversized class if it is there.
    $j( '#dockeditempanel' ).removeClass('oversized_content');
    var panelheight = $j( '#dockeditempanel' ).outerHeight();
    // Set the height of the panel if required and add the oversized class
    if (panelheight > screenheight) {
    	$j( '.dockeditempanel_bd' ).height(screenheight - $j( '.dockeditempanel_hd' ).outerHeight() - $j( 'nav.navbar-fixed-top' ).height() - 10);
    	$j( '.dockeditempanel_bd' ).width(300);
    	$j( '#dockeditempanel' ).addClass('oversized_content');
    }
    // Set the scrolltop of the panel to what it was before we started.
    if (scrolltop) {
    	$j( '.dockeditempanel_bd' ).scrollTop(scrolltop);
    }
}

function generateElement(parent, element, elementid, elementclass, content) {
	var ele = document.createElement(element);
	if(elementid) {ele.id = elementid;}
	if(elementclass) {ele.className = elementclass;}
	if(content) {ele.innerHTML = content;}
	return parent.appendChild(ele);
}

function getElementsByClass(searchClass, domNode, tagName) {
    if (domNode == null) domNode = document;
    if (tagName == null) tagName = '*';
    var el = new Array();
    var tags = domNode.getElementsByTagName(tagName);
    var tcl = " "+searchClass+" ";
    for(i=0,j=0; i<tags.length; i++) {
        var test = " " + tags[i].className + " ";
        if (test.indexOf(tcl) != -1)
        el[j++] = tags[i];
    }
    return el;
}

function hasClass(element, cls) {
	return (' ' + element.className + ' ').indexOf(' ' + cls + ' ') > -1;
}

function toggle(divid) {
	var state = document.getElementById(divid).style.display;
	if (state=="none") {
		newstate = "block";
	} else {
		newstate = "none";
	}
	document.getElementById(divid).style.display = newstate;
}

/* FULLSCREEN TOGGLE */
function theme_cornerstone_fullscreen_toggle() {
    var cu_body = document.body;
    if(cu_body.className.indexOf("fullscreen") != -1) {
    	cu_body.className = cu_body.className.replace( /(?:^|\s)fullscreen(?!\S)/g , '' );
    	togglestatus = "";
    	cufullscreen = 0;
    } else {
    	togglestatus = " fullscreen";
    	cufullscreen = 1;
    	cu_body.className += togglestatus;
    }
	M.util.set_user_preference('cornerstone_fullscreen', cufullscreen);	
}

/* TOGGLE COURSE ALERTS ON MY MOODLE PAGE */
function theme_cornerstone_coursealert_toggle(button) {
	var coursebox = button.parentNode.parentNode.parentNode;
	var courseassignments = getElementsByClass("collapsibleregion", coursebox, "div");
	var assignmentinfo = courseassignments[0].parentNode.parentNode;

	if (button.getAttribute("alt") === "Hide Activity Reminders") {
		for (var i=0; i < courseassignments.length; i++) {
			courseassignments[i].className += " collapsed";
			courseassignments[i].style.height = "0px";	
		}
		button.setAttribute("alt", "Show Activity Reminders");
		assignmentinfo.style.display = "none";
	} else {
		for (var i=0; i < courseassignments.length; i++) {
			courseassignments[i].className = courseassignments[i].className.replace( /(?:^|\s)collapsed(?!\S)/g , '' );
			courseassignments[i].style.height = "auto";
		}
		button.setAttribute("alt", "Hide Activity Reminders");
		assignmentinfo.style.display = "block";
	}
}

/* Gradebook highlighting of rows/columns as user hovers over cells */
$j('tr').mouseover(function(){
	var valueOfTr = $j(this).attr('data-uid');
    $j('div').find("[data-uid='" + valueOfTr + "']").css("background-color","#ffff00");
});
$j('tr').mouseleave(function(){
	var valueOfTr = $j(this).attr('data-uid');
    $j('div').find("[data-uid='" + valueOfTr + "']").css("background-color","#ffffff");
});
$j('td').mouseover(function(){
	var valueOfTd = $j(this).attr('data-itemid');
	if ( $j(".i" + valueOfTd).hasClass("overridden") || $j(".i" + valueOfTd).hasClass("ajaxoverridden") ) {
		$j(".i" + valueOfTd).css({ "background-color" : "#ffff00" });
		$j(".i" + valueOfTd + ".overridden").css({ "background-color" : "#7cba00" });
		$j(".i" + valueOfTd + ".ajaxoverridden").css({ "background-color" : "#7cba00" });
	} else {
		$j(".i" + valueOfTd).css({ "background-color" : "#ffff00" });
	}
});
$j('td').mouseleave(function(){
	var valueOfTd = $j(this).attr('data-itemid');
	//$(".i" + valueOfTd).removeAttr('style');
	$j(".i" + valueOfTd).css('background-color','');
});

$j('li.dropdown').click(function(){
	$j('ul.dropdown-menu').css('max-height', ($j(window).height() - $j('ul.dropdown-menu').offset().top) - $j('#cu-navbar').height() + 'px');
});