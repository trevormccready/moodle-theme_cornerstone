<?php
$settings = null;

defined('MOODLE_INTERNAL') || die;

	global $PAGE;

	$ADMIN->add('themes', new admin_category('theme_cornerstone', 'Cornerstone'));
	
	// IDENTITY OPTIONS
	$temp = new admin_settingpage('theme_cornerstone_org',  'Identity');
    // Organization Name
	$name = 'theme_cornerstone/orgname';
    $title = 'Organization Name';
    $description = 'Default display name of this organization.';
    $default = 'CU';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_CLEAN, 30);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);  
    
    $name = 'theme_cornerstone/website';
    $title = 'Website URL';
    $description = 'Fully-qualified domain name with path to website';
    $default = 'https://www.cornerstone.edu/';
    $setting = new admin_setting_configtext($name,$title,$description,$default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Visible Course Categories
    $name = 'theme_cornerstone/coursecategories';
    $title = get_string('coursecategories','theme_cornerstone');
    $description = get_string('coursecategoriesdesc', 'theme_cornerstone');
    $default = 0;
    $description = $description . '<br/><a href="' . $CFG->wwwroot . '/course/management.php" target="_blank">View the Moodle Course Manager <i class="fa fa-external-link"></i></a>';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_CLEAN, 30);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    $ADMIN->add('theme_cornerstone',$temp);
    
    // NAVIGATION OPTIONS
    $temp = new admin_settingpage('theme_cornerstone_nav',  'Navigation');     
    /* Resources Navigation Menu Dropdown */
    $name = 'theme_cornerstone/showresources';
    $title = 'Show Resources Menu';
    $description = 'Display the Resources dropdown menu in the site navigation.';
    $default = true;
    $setting = new admin_setting_configcheckbox($name,$title,$description,$default,true,false);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
  
    // SUPPORT LINKS
    $temp->add(new admin_setting_heading('theme_cornerstone_nav','Help Links',format_text('Links to support resources'), FORMAT_MARKDOWN));
    /* Live Support */
    $name = 'theme_cornerstone/showlivesupport';
    $title = 'Show Live Support Links';
    $description = 'Display information for live support options.';
    $default = true;
    $setting = new admin_setting_configcheckbox($name,$title,$description,$default,true,false);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    /* Turnitin Help Link */
    $name = 'theme_cornerstone/showturnitinhelp';
    $title = 'Show Turnitin Help Link';
    $description = 'Display a link to the Turnitin help.';
    $default = true;
    $setting = new admin_setting_configcheckbox($name,$title,$description,$default,true,false);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // QUICK LINKS
    $temp->add(new admin_setting_heading('theme_cornerstone_nav','Quick Links',format_text('Links to portal and email'), FORMAT_MARKDOWN));
    /* Portal Link and URL */
    $name = 'theme_cornerstone/showportallink';
    $title = 'Show Portal Link';
    $description = 'Display a link to the Portal in the site navigation.';
    $default = true;
    $setting = new admin_setting_configcheckbox($name,$title,$description,$default,true,false);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    $name = 'theme_cornerstone/portalpath';
    $title = 'Portal URL';
    $description = 'Fully-qualified domain name with path to relevant portal page';
    $default = 'https://portal.cornerstone.edu';
    $setting = new admin_setting_configtext($name,$title,$description,$default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    /* Email Link and URL */
    $name = 'theme_cornerstone/showemaillink';
    $title = 'Show Email Link';
    $description = 'Display a link to email in the site navigation.';
    $default = true;
    $setting = new admin_setting_configcheckbox($name,$title,$description,$default,true,false);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    $name = 'theme_cornerstone/emailpath';
    $title = 'Email URL';
    $description = 'Fully-qualified domain name with path to relevant email page';
    $default = 'http://gmail.cornerstone.edu';
    $setting = new admin_setting_configtext($name,$title,$description,$default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    $ADMIN->add('theme_cornerstone',$temp);
    
    // SYSTEM MESSAGES
    $temp = new admin_settingpage('theme_cornerstone_messages',  'System Messages');
    
    /* System alert enable/disable */
    $name = 'theme_cornerstone/showalert';
    $title = 'Show Alert';
    $description = 'Display a message concerning irregularly scheduled maintenance or other important information concerning Moodle availability.';
    $default = false;
    $setting = new admin_setting_configcheckbox($name,$title,$description,$default,true,false);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    /* Maintenance alert message */
    $name = 'theme_cornerstone/alertmessage';
    $title = 'Alert message';
    $description = 'Formatted message to be displayed as an alert at the top of the login and home pages.';
    $default = '';
    $setting = new admin_setting_confightmleditor($name,$title,$description,$default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    /* Maintenance alert message */
    $name = 'theme_cornerstone/alertendtime';
    $title = 'Alert End Date/Time';
    $description = 'Indicate a time when the alert will no longer be displayed (YYYY-MM-DD HH:MM:SS).';
    $default = '';
    $maxlength = '19';
    $setting = new admin_setting_configtext_with_maxlength($name,$title,$description,$default,PARAM_RAW,null,$maxlength);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    $ADMIN->add('theme_cornerstone',$temp);