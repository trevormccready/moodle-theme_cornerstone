<?php
/**
 * Returns an object containing HTML for the areas affected by settings.
 *
 * @param renderer_base $output Pass in $OUTPUT.
 * @param moodle_page $page Pass in $PAGE.
 * @return stdClass An object with the following properties:
 * 		- sitetitle The theme's organization name for the subbranding displayed in the navbar
 * 		- emaillink A link to the organization's email website to appear in the navbar and footer
 * 		- portallink A link to the organization's portal website to appear in the navbar and footer
 */
function theme_cornerstone_get_settings(renderer_base $OUTPUT, moodle_page $PAGE) {
	global $CFG;
	$return = new stdClass;

	if (!empty($PAGE->theme->settings->orgname)) {
		$return->sitetitle = '<div class="sitetitle">'.$PAGE->theme->settings->orgname.'</div>';
		if ($PAGE->theme->settings->orgname == 'CU') {
			$return->subbrand = '';
		} else {
			$return->subbrand = '<div class="subbrand"><span style="font-size: 0.75em; font-style: italic;">for</span> ' . $PAGE->theme->settings->orgname . '</div>';		
		}
	} else {
		$return->sitetitle = 'CU';
	}

	if (!empty($PAGE->theme->settings->showemaillink) && !empty($PAGE->theme->settings->emailpath)) {
		$return->emaillink = '<a href="'.$PAGE->theme->settings->emailpath.'" target="_blank"  title="CU Email"><i class="fa fa-envelope" aria-hidden="false" aria-label="Go to the University email"></i>CU Email</a>';
	}	
	if (!empty($PAGE->theme->settings->showportallink) && !empty($PAGE->theme->settings->portalpath)) {
		$return->portallink = '<a href="'.$PAGE->theme->settings->portalpath.'" target="_blank"  title="CU Portal"><i class="fa fa-university" aria-hidden="false" aria-label="Go to the University portal"></i>CU Portal</a>';
	}

	if(!empty($PAGE->theme->settings->showalert) && !empty($PAGE->theme->settings->alertmessage)) {
		$unixendtime = new DateTime($PAGE->theme->settings->alertendtime);
		if ($unixendtime->getTimestamp() > time()) {
			$return->alert = '<div id="cu-alertbox" class="alert alert-info alert-dismissable fade in" data-endtime="'.$PAGE->theme->settings->alertendtime.'"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><div id="cu-alertmessage">'.$PAGE->theme->settings->alertmessage.'</div></div>';	
		}
	}
	
	/* Allow the updating of the following user preferences, and query their current state */
	user_preference_allow_ajax_update('cornerstone_fullscreen', PARAM_BOOL);
	$return->cufullscreenpreference = get_user_preferences('cornerstone_fullscreen');
	
	return $return;
}

/**
 * Returns an object containing information concerning the course and user.
 *
 * @param moodle_page $PAGE Pass in $PAGE.
 * @return stdClass An object with the following properties:
 * 		- id The course category for the current page
 */
function theme_cornerstone_get_root_category(moodle_page $PAGE) {
	$return = '';
	
	$category = $PAGE->course->category;
	if ($category > 0) {
		$categories = $PAGE->categories[$category]->path;
		$rootcategoryid = explode('/',$categories);
		$rootcategory = $PAGE->categories[$rootcategoryid[1]]->idnumber;
	} else {
		$rootcategory = '';
	}
	$return = strtolower($rootcategory);
	
	return $return;
}

/**
 * Returns an object containing information concerning the course and user.
 *
 * @param renderer_base $OUTPUT Pass in $OUTPUT.
 * @return true/false
 *
 */
function theme_cornerstone_get_course_role(moodle_page $PAGE) {
	global $COURSE, $USER;
	$result = 'student';
	
	return $result;
}

?>