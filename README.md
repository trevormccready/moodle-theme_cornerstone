# README #
UPDATED: 1/4/2017

### VERSION ###
This project was originally launched on July 2, 2016 when Cornerstone University migrated Moodle 2.9 from MoodleRooms (aka Blackboard) commercial hosting to an in-house virtual computing environment on Microsoft Azure. The theme has been incrementally improved and is currently supported with Moodle 3.1.x

This custom theme for Cornerstone University makes use of the following external frameworks:

* Bootstrap v3.3.6 (http://getbootstrap.com)
* FontAwesome 4.6.3 (http://fontawesome.io)
* JQuery v1.12.3 (http://jquery.org)

### Questions? ###

* If you have questions about this theme, please contact Trevor McCready (trevor.mccready@cornerstone.edu).