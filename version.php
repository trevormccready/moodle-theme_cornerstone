<?php
/**
 * @package		theme_cornerstone
 * @copyright	2017 Cornerstone University, www.cornerstone.edu
 * @author 		Trevor McCready
 * @license 	All rights reserved.
 */

defined('MOODLE_INTERNAL') || die;

$plugin->version	=	2019060700;
$plugin->requires	=	2018120300; // Developed and tested for Moodle 3.6
$plugin->release	=	'v3.6+';
$plugin->maturity	=	MATURITY_STABLE;
$plugin->component	=	'theme_cornerstone';
