<?php
$THEME->doctype =	'html5';
//$THEME->yuicssmodules =	array();
$THEME->name	= 	'cornerstone';
$THEME->parents	= 	array();
$THEME->sheets	=	array('bootstrap','font-awesome','bootstrap-moodle','express','cornerstone','cu-dashboard','cu-editor','cu-header','cu-footer','cu-admin','cu-blocks','cu-forms','cu-fullscreen','cu-course','cu-forum','cu-grades','cu-login','cu-messaging','cu-quiz','cu-yui','cu-responsive','cu-print','cu-usertours');
//$THEME->editor_sheets	=	array('editor');
$THEME->javascripts	=	array();
$THEME->javascripts_footer	=	array('moodlebootstrap','jquery32','cornerstone');
$THEME->rendererfactory 	=	'theme_overridden_renderer_factory';
$THEME->enable_dock = true;
$THEME->layouts = 	array(
	'base'	=>	array(
		'file'	=>	'general.php',
		'regions'	=>	array(),
	),
	'standard'	=>	array(
		'file'	=>	'general.php',
		'regions'	=>	array('side-pre'),
		'defaultregion'	=>	'side-pre',
	),
	'admin'		=>	array(
		'file'	=>	'admin.php',
		'regions'	=>	array('side-pre'),
		'defaultregion'	=>	'side-pre',
	),
	'course'	=>	array(
		'file'	=>	'general.php',
		'regions'	=>	array('side-pre'),
		'defaultregion'	=>	'side-pre',
	),
	'coursecategory'	=>	array(
		'file'	=>	'general.php',
		'regions'	=>	array('side-pre'),
		'defaultregion'	=>	'side-pre',
	),
	'embedded'	=>	array(
		'file'	=>	'embedded.php',
		'regions'	=>	'array()',
	),
	'frametop'	=>	array(
		'file'	=>	'general.php',
		'regions'	=>	array(),
		'options'	=>	array('nofooter' => true, 'nocoursefooter'	=>	true),
	),
	'frontpage'	=>	array(
		'file'	=>	'frontpage.php',
		'regions'	=>	array('side-pre','side-post'),
		'defaultregion'	=>	'side-post',
	),
	'incourse'	=>	array(
		'file'	=>	'general.php',
		'regions'	=>	array('side-pre'),
		'defaultregion'	=>	'side-pre',
	),
	'login'	=>	array(
		'file'	=>	'general.php',
		'regions'	=>	array(),
		'options'	=>	array('langmenu'=>true),
	),
	'maintenance'	=>	array(
		'file'	=>	'maintenance.php',
		'regions'	=>	array(),
		'options'	=>	array('noblocks'=>true,	'nofooter'=>true, 'nonavbar'=>true, 'nocustommenu'=>true, 'nocourseheaderfooter'=>true),
	),
	'mydashboard'	=>	array(
		'file'	=>	'mydashboard.php',
		'regions'	=> array('side-pre','side-post'),
		'defaultregion'	=>	'side-post',
	),
	'mypublic'	=>	array(
		'file'	=>	'general.php',
		'regions'	=> 	array('side-pre'),
		'defaultregion'	=>	'side-pre',
	),
	'popup'	=>	array(
			'file'	=>	'general.php',
			'regions'	=>	array(),
			'options'	=>	array('nofooter'=>true, 'nonavbar'=>true, 'nocustommenu'=>true, 'nologininfo'=>true, 'nocourseheaderfooter'=>true),
	),		
	'print'	=>	array(
		'file'	=>	'general.php',
		'regions'	=>	array(),
		'options'	=>	array('noblocks'=>true, 'nofooter'=>true, 'nonavbar'=>true, 'nocustommenu'=>true, 'nocourseheaderfooter'=>true),
	),
	'redirect'	=>	array(
		'file'	=>	'redirect.php',
		'regions'	=>	array(),
		'options'	=>	array('noblocks'=>true,'nofooter'=>true, 'nonavbar'=>true, 'nocustommenu'=>true, 'nocourseheaderfooter'=>true),
	),
	'report'	=>	array(
		'file'	=>	'general.php',
		'regions'	=>	array('side-pre'),
		'defaultregion' => 'side-pre',
	),
	'secure'	=>	array(
		'file'	=>	'general.php',
		'regions'	=>	array('side-pre'),
		'defaultregion'	=>	'side-pre',
		'options'	=>	array('nofooter'=>true, 'nonavbar'=>true, 'nocustommenu'=>true, 'nologinlinks'=>true, 'nocourseheaderfooter'=>true),
	),
);
